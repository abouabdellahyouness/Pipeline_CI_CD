# Contenu de test_app.py

import pytest


def test_index_page(client):
    """Test pour vérifier que la page d'accueil est accessible et renvoie le code de statut 200."""
    response = client.get('/')
    assert response.status_code == 200


def test_index_content(client):
    """Test pour vérifier que la page d'accueil contient le texte attendu."""
    response = client.get('/')
    assert b'Bonjour Mr LARAQUI, on est le' in response.data
    assert b'id="date"' in response.data
    assert b'id="heure"' in response.data
